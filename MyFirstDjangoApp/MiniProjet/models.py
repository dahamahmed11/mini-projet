from django.db import models

# Create your models here.

class FormationInfo(models.Model):
    formation_name = models.CharField(max_length=100)
    formation_etat = models.CharField(max_length=100)
    formation_CreatedDate = models.CharField(max_length=50)  
    formation_detail = models.TextField(max_length=500)

    def __str__(self):
        return self.formation_name