from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import FormationInfo
# Create your views here.
def index (request):
    all_formations = FormationInfo.objects.all()
    return render(request,"MiniProjet/index.html",{'Formations' : all_formations})

def AddFormation (request):
    return render(request,"MiniProjet/add_formation.html")

def add_formation_form_submission (request):
    print("Hello form is submitted.")
    formation_name=request.POST["formation_name"]
    formation_CreatedDate=request.POST["formation_CreatedDate"]
    formation_detail=request.POST["formation_detail"]
    formation_etat=request.POST["formation_etat"]
    formation_info=FormationInfo(formation_name=formation_name,formation_CreatedDate=formation_CreatedDate,formation_detail=formation_detail,formation_etat=formation_etat)

    formation_info.save()
    all_formations = FormationInfo.objects.all()
    return render(request,"MiniProjet/index.html",{'formations':all_formations})